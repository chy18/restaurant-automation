The following information/implemnetation belongs to Rutgers University ECE Software Engineering Group 7 from Spring 2021

User may clone the program/project through the following info:
	link: https://bitbucket.org/chy18/restaurant-automation/
	git clone https://chy18@bitbucket.org/chy18/restaurant-automation.git

THIS IS A README FILE OF HOW TO APPLY THE UNIT TESTS TOWARD THE PROGRAM

The program should be tested by following the guidance with the documents below:

- unit_testing_cook.pdf
- unit_testing_chef.pdf
- unit_testing_manager.pdf
- unit_testing_receptionist.pdf
- unit_testing_waiter.pdf

Each document represents a list of functionalities regarding its position whereas the unit testing should be
done by following the instructions within the document. Each document contains a list of tests which each 
test corresponds to a specific functionality within such position. Inspection toward the program should be done 
by going through the "bullet points" of the document which shows the specifications of the interface/display. 
Further testing/inspection details may vary across different functionalities and user positions.