The following information/implemnetation belongs to Rutgers University ECE Software Engineering Group 7 from Spring 2021

User may clone the program/project through the following info:
	link: https://bitbucket.org/chy18/restaurant-automation/
	git clone https://chy18@bitbucket.org/chy18/restaurant-automation.git

THIS IS A README FILE OF THE DATA COLLECTION FROM THE TESTING OF THE PROGRAM

The following documents contain the sample data that are applied during the testing:

- data_collection_cleanliness.pdf
- data_collection_customer.pdf
- data_collection_employee.pdf
- data_collection_inventory.pdf
- data_collection_menu.pdf
- data_collection_order.pdf
- data_collection_reservation.pdf
- data_collection_survey.pdf

The data information above is retrieved/exported through MySQL directly which represents the raw data.


