The following information/implemnetation belongs to Rutgers University ECE Software Engineering Group 7 from Spring 2021

User may clone the program/project through the following info:
	link: https://bitbucket.org/chy18/restaurant-automation/
	git clone https://chy18@bitbucket.org/chy18/restaurant-automation.git

THIS IS A README FILE OF HOW TO APPLY THE INTEGRATION TESTS TOWARD THE PROGRAM

The program should be tested by following the guidance with the documents below:

- integration_testing_cleanliness.pdf
- integration_testing_customer.pdf
- integration_testing_employee.pdf
- integration_testing_inventory.pdf
- integration_testing_menu.pdf
- integration_testing_order.pdf
- integration_testing_reservation.pdf
- integration_testing_survey.pdf

Each document consists of a testing that focuses on certain database manipulation which may be utilizied by different 
functionalities across divisions and employment types. The testing result is records within each document whereas 
the data that are obtained throughout the testing is also recorded.


