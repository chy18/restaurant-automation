The following information/implemnetation belongs to Rutgers University ECE Software Engineering Group 7 from Spring 2021

User may clone the program/project through the following info:
	link: https://bitbucket.org/chy18/restaurant-automation/
	git clone https://chy18@bitbucket.org/chy18/restaurant-automation.git

THIS IS A README FILE OF HOW TO RUN THE PROGRAM ON YOUR COMPUTER

The restaurant-automation software program/system is integrated as 
a web-server which can be accessed through web broswers from various 
devices within a mutual network domain. The following instructions 
show user how to execute the program on one's computer:

1. 	Make sure the entire folder "restaurant-automation" is copied/pasted 
	or cloned to a local directory on the computer.
2.	Click on the folder "1_code"
3.	Double-click on the file "usbwebserver.exe" to execute the program
4. 	(If any message pops up, please allow full accesses to the computer
	for the program to be operated properly)
5.	When the USBWebserver's window is opened, ensure that the "green check
	mark" is shown next to both "Apache" and "Mysql"
6. 	Click on "Localhost" to enter the program, the home page should be 
	opened in a default browser
7.	User may operate the system by log in with one's credentials and the 
	provided functionals will be shown

The following tutorial shows how to access the web pages from other devices 
beside on localhost/local directory on user's computer:

1.	Make sure the computer is connected to a local network
2.	Go to Windows Firewall settings and click on the tab "advanced settings"
	on the left
3.	In the tab "Inbound Rules" on the right, click on “New Rule” option and 
	a wizard window will pop up
4. 	In the wizard there are five steps. In the first step user have to select 
	type of rule. Since the program is to allow traffic for a specific 
	port. Therefore select the "port" option and click "next"
5.	Now choose “Specific local ports” option and enter port number that the 
	web server is listening on (defalt: 8080) and click "next"
6.	Now user has to choose an action, therefore choose “Allow the connection”
	and click "next"
7.	Now specify when does this rule should apply, therefore choose all of 
	the options and click "next"
8. 	Now give a name for this rule (user can also specify a description for 
	this rule which is not mandatory)
9.	Finally click on the "Finish" button to save the new rule
10. 	For Windows User: open "Command Propt" and type "ipconfig" to obtain the
	current IP address, please record this IP address down
11.	(Other OS users may obtain their IP addresses in various ways which can be
	searched up online through search engines)

At this moment the webserver is established through an open IP address, thus 
please follow the instructions below to successfully access the web pages from 
user's desire device

1.	Make sure that the device user wishes to operate on is connected to 
	the same local network as the server (computer from above)
2.	Open the web browser and enter the following HTTP:
		"(IP ADDRESS):(PORT #)"
	eg: if the IP address is 192.168.1.1 and the port # is 8080, then:
		"192.168.1.1:8080"
	(The IP address and the port # may vary accross different servers/computers
	depending on their specifications)
3.	The Home-Page should be loaded and user may operate the system as usual

The following "default" credentials may be entered to access their corresponding functionality pages:
	- username: waiter		(password: waiter)
	- username: receptionist	(password: receptionist)
	- username: cook		(password: cook)
	- username: chef		(password: chef)
	- username: manager		(password: manager)

