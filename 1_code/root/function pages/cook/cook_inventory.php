<!-- written by: Zerui Li, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<html>
<head>
  <title>Cook: Inventory</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>

<body>
<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Inventory</legend>

</fieldset>
</form>



<form class="form-horizontal" method="post">
<?php
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM inventory_db");

  $i = 0;
  while ($rows = $resultSet->fetch_assoc()){
    $y = $rows['matName'];
    $matCount = $rows['matQuantity'];
    $m = 'mat';
    $m .= strval($i);
    $c = 'count';
    $c .=strval($i);

    echo "
<div class='form-group'>
  <label class='col-md-4 control-label' for='ingredient1'>Ingredient name:</label>  
  <div class='col-md-4'>
  <input id='$m' name='$m' type='text' placeholder='' class='form-control input-md' readonly>
    
  </div>
</div>

<div class='form-group'>
  <label class='col-md-4 control-label' for='quantity'>Quantity:</label>  
  <div class='col-md-1'>
  <input id='$c' name='$c' type='number' placeholder='' class='form-control input-md' readonly>
    
  </div>
</div>

<script>
document.getElementById('$m').value = '$y';
document.getElementById('$c').value = '$matCount';
</script>

    ";
    $i = $i + 1;
  }

?>


</form>
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="exit"></label>
  <div class="col-md-4">
    <form class="form-horizontal" action = "../../position pages/cook.php">
    <button id="exit" name="exit" class="btn btn-primary">exit</button>
    </form>
  </div>
</div>

</body>
</html>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST"){
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM inventory_db");
  $i = 0;
  echo "<script>
  document.getElementById('admin').disabled = true;
  document.getElementById('save').disabled = false;
  </script>";
  while ($rows = $resultSet->fetch_assoc()){
    $c = 'count';
    $c .=strval($i);

    echo"
    <script>
      document.getElementById('$c').readOnly = false;
    </script>

    ";
    $i = $i + 1;
  }
}
?>

