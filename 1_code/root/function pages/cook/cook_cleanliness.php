<!-- written by: Zerui Li, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<html>
<head>
  <title>Cook: Cleanliness</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>

<body>


<!-- Form Name -->
<legend>Cleanliness</legend>

<form class="form-horizontal" method="post">

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="monday">Duty for Monday</label>  
  <div class="col-md-4">
  <input id="Monday" name="Monday" type="text" placeholder="" class="form-control input-md" readonly>
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Tuesday">Duty for Tuesday</label>  
  <div class="col-md-4">
  <input id="Tuesday" name="Tuesday" type="text" placeholder="" class="form-control input-md" readonly>
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Wednesday">Duty for Wednesday</label>  
  <div class="col-md-4">
  <input id="Wednesday" name="Wednesday" type="text" placeholder="" class="form-control input-md" readonly>
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Thursday">Duty for Thursday</label>  
  <div class="col-md-4">
  <input id="Thursday" name="Thursday" type="text" placeholder="" class="form-control input-md" readonly>
    
  </div>
</div>




<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Friday">Duty for Friday</label>  
  <div class="col-md-4">
  <input id="Friday" name="Friday" type="text" placeholder="" class="form-control input-md" readonly>
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Saturday">Duty for Saturday</label>  
  <div class="col-md-4">
  <input id="Saturday" name="Saturday" type="text" placeholder="" class="form-control input-md" readonly>
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Sunday">Duty for Sunday</label>  
  <div class="col-md-4">
  <input id="Sunday" name="Sunday" type="text" placeholder="" class="form-control input-md" readonly>
    
  </div>
</div>



</form>

<form class="form-horizontal" action = "../../position pages/cook.php">
<fieldset>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="exit"></label>
  <div class="col-md-4">
    <button id="exit" name="exit" class="btn btn-primary">Exit</button>
  </div>
</div>

</fieldset>
</form>

<?php
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM cleanliness_db");

  $i = 0;
  while ($rows = $resultSet->fetch_assoc()){
    $y = $rows['day'];
    $name = $rows['name'];

    echo "
    <script>
      document.getElementById('$y').value = '$name';
    </script>

    ";
    $i = $i + 1;

  }
?>


</body>
</html>

<?php
  if ($_SERVER["REQUEST_METHOD"] == "POST"){
    echo"
    <script>
      document.getElementById('Monday').readOnly = false;
      document.getElementById('Tuesday').readOnly = false;
      document.getElementById('Wednesday').readOnly = false;
      document.getElementById('Thursday').readOnly = false;
      document.getElementById('Friday').readOnly = false;
      document.getElementById('Saturday').readOnly = false;
      document.getElementById('Sunday').readOnly = false;
      document.getElementById('modify').disabled = true;
      document.getElementById('confirm').disabled = false;
    </script>

    ";
  }

?>