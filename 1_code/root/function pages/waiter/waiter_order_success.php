<!-- written by: Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<?php

include "../../includes/php_scripts/db_connect.php";
if ($_SERVER["REQUEST_METHOD"] == "POST"){
	$resultSet = $mysqli->query("SELECT * FROM menu_db");

	date_default_timezone_set('America/New_York');
	$i = 0;
	$orderDishes = "";
	$orderTime = date("Y-m-d");
	$orderTime .= " ";
	$orderTime .= date("H:i:s");
	$orderEstimate = 0;
	$orderDishes = "";
	$orderPrice = 0;
	$isCurrent = 1;
	while ($rows = $resultSet->fetch_assoc()){
		$c = "count";
		$c .=strval($i);
		if (test_input($_POST["$c"]) > 0){
			$orderDishes .= $rows['dishName'];
			$orderDishes .= " x";
			$orderDishes .= test_input($_POST["$c"]);
			$orderDishes .= ", ";
		}
		$orderEstimate = $orderEstimate + test_input($_POST["$c"])*$rows['dishEstimate'];
		$orderPrice = $orderPrice + test_input($_POST["$c"])*$rows['dishPrice'];
		$i = $i + 1;
	}
	session_start();
	$_SESSION['time'] = $orderEstimate;
	$sql = "INSERT INTO order_db (orderID, orderTime, orderEstimate, orderDishes, orderPrice, isCurrent) VALUES (NULL, '$orderTime', '$orderEstimate', '$orderDishes', '$orderPrice', '$isCurrent')";
	$result = $mysqli->query($sql);
}

function test_input($data) {
	$data = trim($data);
  	$data = stripslashes($data);
  	$data = htmlspecialchars($data);
  	return $data;
}

?>


<html>
<head>
	<title>Order: Confirmation</title>
</head>

<body>
	<h1>
		<?php
		$x = $_SESSION['time'];
		echo "The order has be completed, the estimation time is: $x minutes";

		?>
	</h1>
	<form method="post" class="form-horizontal" action = "../../position pages/waiter.php">
	<button type="submit" id="submit" name="submit" class="btn btn-primary">Exit</button>
</body>


</html>