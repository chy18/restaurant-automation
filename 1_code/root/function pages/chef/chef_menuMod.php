<!-- written by: Xiaotian Zhou, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<html>
<head>
  <title>Chef: Menu Modification</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<legend>Menu Modification</legend>

<form class="form-horizontal" action = "../../position pages/chef.php">
  <fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="return" ></label>
  <div class="col-md-4">
    <button id="return" name="return" class="btn btn-primary">Exit</button>
  </div>
</div>

</fieldset>
</form>

<form class="form-horizontal" action="chef_menuModAdd.php">
<fieldset>
<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewName">Name of the dish: </label>
  <div class="col-md-4">
    <input id="dishNewName" name="dishNewName" type="search" placeholder="" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewStyle">Style of the dish: </label>
  <div class="col-md-4">
    <input id="dishNewStyle" name="dishNewStyle" type="search" placeholder="" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewPrice">Price of the dish:</label>
  <div class="col-md-4">
    <input id="dishNewPrice" name="dishNewPrice" type="search" placeholder="" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewEstimate">Estimate wait time of the dish:</label>
  <div class="col-md-4">
    <input id="dishNewEstimate" name="dishNewEstimate" type="search" placeholder="" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="orderSubmit"></label>
  <div class="col-md-4">
    <button id="orderSubmit" name="orderSubmit" class="btn btn-success">Add</button>
  </div>
</div>

</fieldset>
</form>

<form class="form-horizontal" method="post" action="chef_menuModDel.php">
  <fieldset>
<div class="form-group">
  <label class="col-md-4 control-label" for="dishChoice">Select dish</label>
  <div class="col-md-4">
    <select id="dishChoice" name="dishChoice" class="form-control">
      <?php
      include "../../includes/php_scripts/db_connect.php";
      $resultSet = $mysqli->query("SELECT * FROM menu_db");
      $i = 0;
      while ($rows = $resultSet->fetch_assoc()){
        $dish = $rows["dishName"];
        $id = $rows["dishID"];
        echo"
          <option value = '$id'>$dish</option>
        ";
      }


      ?>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishDelete"></label>
  <div class="col-md-4">
    <button id="dishDelete" name="dishDelete" class="btn btn-danger">Delete</button>
  </div>
</div>

</fieldset>
</form>



</body>
</html>