<!-- written by: Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<?php

include "../../includes/php_scripts/db_connect.php";
$id = test_input($_POST["dishChoice"]);
$sql = "DELETE FROM menu_db WHERE dishID = $id";
$resultSet = $mysqli->query($sql);

echo "<h2>Trying to delete a dish: <br> dishID: $id <br><h2>";

echo "<h3>menu updated!<h3>";

function test_input($data) {
  $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<html>
<head>
	<title>Menu: Item Deleted</title>
</head>
<body>
	<form method="post" class="form-horizontal" action="../../position pages/chef.php">
		<button type="submit" id="submit" name="submit" class="btn btn-primary">Exit</button>
	</form>
</body>
</html>
