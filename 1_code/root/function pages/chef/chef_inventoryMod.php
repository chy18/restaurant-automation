<!-- written by: Xiaotian Zhou, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<html>
<head>
  <title>Chef: Inventory Modification</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<legend>Inventory Modification</legend>

<form class="form-horizontal" action = "../../position pages/chef.php">
  <fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="return" ></label>
  <div class="col-md-4">
    <button id="return" name="return" class="btn btn-danger">Cancel</button>
  </div>
</div>

</fieldset>
</form>

<form class="form-horizontal" action="chef_inventoryModAdd.php">
<fieldset>
<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewName">Name of the ingredient: </label>
  <div class="col-md-4">
    <input id="itemNewName" name="itemNewName" type="search" placeholder="" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewStyle">Initial amount: </label>
  <div class="col-md-4">
    <input id="itemNewNum" name="itemNewNum" type="number" placeholder="" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="orderSubmit"></label>
  <div class="col-md-4">
    <button id="orderSubmit" name="orderSubmit" class="btn btn-success">Add</button>
  </div>
</div>

</fieldset>
</form>
</body>
</html>