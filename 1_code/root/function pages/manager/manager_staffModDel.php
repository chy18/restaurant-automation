<!-- written by: Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<?php

include "../../includes/php_scripts/db_connect.php";
$id = test_input($_POST["staff"]);
$sql = "DELETE FROM employee_db WHERE employeeID = $id";
$resultSet = $mysqli->query($sql);

echo "<h2>Trying to remove a Staff: <br> employeeID: $id <br><h2>";

echo "<h3>Employee List updated!<h3>";

function test_input($data) {
  $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<html>
<head>
	<title>Employee: Removed</title>
</head>
<body>
	<form method="post" class="form-horizontal" action="../../position pages/manager.php">
		<button type="submit" id="submit" name="submit" class="btn btn-primary">Exit</button>
	</form>
</body>
</html>
