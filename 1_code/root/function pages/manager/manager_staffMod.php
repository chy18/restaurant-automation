<!-- written by: Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<html>
<head>
  <title>Manager: Staff Modification</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<legend>Staff Modification</legend>

<form class="form-horizontal" action = "../../position pages/manager.php">
  <fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="return" ></label>
  <div class="col-md-4">
    <button id="return" name="return" class="btn btn-primary">Exit</button>
  </div>
</div>

</fieldset>
</form>

<form class="form-horizontal" action="manager_staffModAdd.php">
<fieldset>
<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewName">Staff Name: </label>
  <div class="col-md-4">
    <input id="name" name="name" type="search" placeholder="" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewName">Work Position: </label>
  <div class="col-md-4">
    <select id="position" name="position" class="form-control">
      <option value = "waiter">waiter</option>
      <option value = "receptionist">receptionist</option>
      <option value = "cook">cook</option>
      <option value = "chef">chef</option>
      <option value = "manager">manager</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewName">Username: </label>
  <div class="col-md-4">
    <input id="username" name="username" type="search" placeholder="" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishNewName">Password: </label>
  <div class="col-md-4">
    <input id="password" name="password" type="password" placeholder="" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="orderSubmit"></label>
  <div class="col-md-4">
    <button id="orderSubmit" name="orderSubmit" class="btn btn-success">Add</button>
  </div>
</div>

</fieldset>
</form>

<form class="form-horizontal" method="post" action="manager_staffModDel.php">
  <fieldset>
<div class="form-group">
  <label class="col-md-4 control-label" for="dishChoice">Select staff</label>
  <div class="col-md-4">
    <select id="staff" name="staff" class="form-control">
      <?php
      include "../../includes/php_scripts/db_connect.php";
      $resultSet = $mysqli->query("SELECT * FROM employee_db");
      $i = 0;
      while ($rows = $resultSet->fetch_assoc()){
        $name = $rows["employeeName"];
        $id = $rows["employeeID"];
        echo"
          <option value = '$id'>$name</option>
        ";
      }


      ?>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dishDelete"></label>
  <div class="col-md-4">
    <button id="dishDelete" name="dishDelete" class="btn btn-danger">Delete</button>
  </div>
</div>

</fieldset>
</form>



</body>
</html>