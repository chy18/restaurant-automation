<!-- written by: Haoxiang Zhang, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<!DOCTYPE html>
<html style="font-size: 16px;">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Manager: Staff Information</title>
    <link rel="stylesheet" href="nicepage.css" media="screen">
<link rel="stylesheet" href="staff-info.css" media="screen">
    <script class="u-script" type="text/javascript" src="jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="nicepage.js" defer=""></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
    <meta name="generator" content="Nicepage 3.11.0, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    
    
    <script type="application/ld+json">{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": "Site1",
		"url": "index.html"
}</script>
    <meta property="og:title" content="staff info">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
    <link rel="canonical" href="index.html">
    <meta property="og:url" content="index.html">
  </head>

<form class="form-horizontal" method="post" action="../../position pages/manager.php">
    <button id="return" name="return" class="btn btn-primary">Return</button>

</form>

  <body class="u-body"><header class="u-clearfix u-header u-header" id="sec-a76f"><div class="u-clearfix u-sheet u-sheet-1"></div></header>
<h1>Staff Information</h1>

<h3>     
    <form class="form-horizontal" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <div class="form-group">
      <div class="u-align-center">
    <button id="admin" name="admin" class="btn btn-primary">Click to modify</button>
      </div>
    </div>
    </form>      

      <form class="form-horizontal" method="post" action="manager_staffInfo_success.php">

    <section class="u-align-center u-clearfix u-section-1" id="sec-114a">
      <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-table u-table-responsive u-table-1">
          <table class="u-table-entity u-table-entity-1">
            <colgroup>
              <col width="25%">
              <col width="25%">
              <col width="25.000000000000007%">
              <col width="24.900000000000006%">
            </colgroup>
            <thead class="u-black u-table-header u-table-header-1">
              <tr style="height: 47px;">
                <th class="u-border-1 u-border-black u-table-cell">Name</th>
                <th class="u-border-1 u-border-black u-table-cell">&nbsp;Work Position</th>
                <th class="u-border-1 u-border-black u-table-cell">ID</th>
                <th class="u-border-1 u-border-black u-table-cell">Username</th>
                <th class="u-border-1 u-border-black u-table-cell">Password</th>
              </tr>
            </thead>
            <tbody class="u-table-body">

<?php
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM employee_db");
  $i = 0;
  while ($rows = $resultSet->fetch_assoc()){
    $name = $rows['employeeName'];
    $position = $rows['employeeType'];
    $id = $rows['employeeID'];
    $username = $rows['employeeUsername'];
    $password = $rows['employeePassword'];
    $b = 'position';
    $b .=strval($i);
    $c = 'id';
    $c .= strval($i);
    $d = 'username';
    $d .=strval($i);
    $e = 'password';
    $e .=strval($i);

    echo"
<tr style='height: 75px;'>
                <td class='u-border-1 u-border-grey-30 u-table-cell'>$name</td>
                <td class='u-border-1 u-border-grey-30 u-table-cell'>
                  <input id='$b' name='$b' type='search' placeholder=''class='form-control input-md' readonly>
                </td>
                <td class='u-border-1 u-border-grey-30 u-table-cell'>
                  <input id='$c' name='$c' type='number' placeholder='' class='form-control input-md' readonly>
                </td>
                <td class='u-border-1 u-border-grey-30 u-table-cell'>
                  <input id='$d' name='$d' type='search' placeholder='' class='form-control input-md' readonly>
                </td>
                <td class='u-border-1 u-border-grey-30 u-table-cell'>
                  <input id='$e' name='$e' type='password' placeholder='' class='form-control input-md' disabled>
                </td>
              </tr>

              <script>
              document.getElementById('$b').value = '$position';
              document.getElementById('$c').value = '$id';
              document.getElementById('$d').value = '$username';
              document.getElementById('$e').value = '$password'
              </script>


    ";
    $i = $i + 1;
  }

?>

              



            </tbody>
          </table>
        </div>
      </div>
    </section>

       <div class="form-group">
      <div class="u-align-center">
    <button id="save" name="save" class="btn btn-success" disabled>Save</button>
      </div>
    </div>
    </form>
    
    
    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-c170"><div class="u-clearfix u-sheet u-sheet-1">
        <p class="u-small-text u-text u-text-variant u-text-1"></p>
      </div></footer>
    </h3>
  </body>
</html>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST"){
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM employee_db");
  $i = 0;
  echo "
  <script>
  document.getElementById('admin').disabled = true;
  document.getElementById('save').disabled = false;
  </script>
  ";
  while ($rows = $resultSet->fetch_assoc()){
    $d = 'username';
    $d .=strval($i);
    $e = 'password';
    $e .=strval($i);

    echo"
    <script>
      document.getElementById('$d').readOnly = false;
      document.getElementById('$e').disabled = false;
    </script>
    ";
    $i = $i + 1;
  }

}

?>