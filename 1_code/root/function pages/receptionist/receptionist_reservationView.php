<!-- written by: Zerui Li, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<html>
<head>
  <title>Receptionist: View/Cancel Reservation</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>


<body>
<fieldset>

<!-- Form Name -->
<legend>View/Cancel Reservation</legend>

<!-- Select Basic -->
<form method = "post" class="form-horizontal", action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<div class="form-group">
  <label class="col-md-4 control-label" for="chooseReservation">Choose Reservation Name: </label>
  <div class="col-md-4">

    <?php
        include "../../includes/php_scripts/db_connect.php";
        $resultSet = $mysqli->query("SELECT * FROM reservation_db");
      ?>

    <select id="chooseReservation" name="chooseReservation" class="form-control">

      <?php
        while ($rows = $resultSet->fetch_assoc()) {
        $firstName = $rows['firstName'];
          echo "<option value = '$firstName'>$firstName</option>";
        }
      

      ?>

    </select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="confirm"></label>
  <div class="col-md-4">
    <button type = "submit" id="confirm" name="confirm" class="btn btn-primary">Confirm</button>
  </div>
</div>
</form>

<!-- Button -->
<form class="form-horizontal", action = "../../position pages/receptionist.php">
<div class="form-group">
  <label class="col-md-4 control-label" for="cancel"></label>
  <div class="col-md-4">
    <button type = "submit" id="cancel" name="cancel" class="btn btn-danger">Cancel</button>
  </div>
</div>
</form>

<!-- Textarea -->
<form class="form-horizontal", action = "receptionist_reservationView_success.php">
<div class="form-group">
  <label class="col-md-4 control-label" for="displayInfo"></label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="displayInfo" name="displayInfo" rows="20" cols="100" readonly></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="cancelReservation"></label>
  <div class="col-md-4">
    <button id="cancelReservation" name="cancelReservation" class="btn btn-danger" disabled>Cancel Reservation</button>
  </div>
</div>
</form>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $firstName = test_input($_POST["chooseReservation"]);
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM reservation_db");
  while ($rows = $resultSet->fetch_assoc()) {
    if ((strcmp($rows['firstName'], $firstName) == 0)){
      session_start();
      $_SESSION['reservationID'] = $rows['reservationID'];
      $phoneNum = $rows['phoneNum'];
      $scheduleTime = $rows['scheduleTime'];
      echo "
        <script>
        document.getElementById('displayInfo').value='Name: $firstName,\\nPhone #: $phoneNum, \\nScheduled Time: $scheduleTime' ;
        document.getElementById('cancelReservation').disabled = false;
        </script>

      ";
      break;
    }
  }
}
function test_input($data) {
  $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

</fieldset>
</body>
</html>