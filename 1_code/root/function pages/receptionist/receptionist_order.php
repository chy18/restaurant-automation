<!-- written by: Zerui Li, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<HTML>
 <HEAD>
  <TITLE>Receptionist: Order</TITLE>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

 </HEAD>
 
 <BODY>
<form method="post" class="form-horizontal" action = "../../position pages/receptionist.php">
 <form class="form-horizontal">
<fieldset>

<legend>Order</legend>
<!--RETURN Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="return" ></label>
  <div class="col-md-4">
  	
    <button id="return" name="return" class="btn btn-danger">Cancel</button>

  </div>
</div>
<br><br>
</fieldset>
</form>
	</form>


<form method="post" class="form-horizontal" action = "receptionist_order_success.php">

<?php
	include "../../includes/php_scripts/db_connect.php";
	$resultSet = $mysqli->query("SELECT * FROM menu_db");

	$i = 0;
	while ($rows = $resultSet->fetch_assoc()){
		$y = $rows['dishName'];
		$d = 'dish';
		$d .= strval($i);
		$c = 'count';
		$c .=strval($i);

	echo "

	<div class='form-group'>
		<label class='col-md-4 control-label' for='return'></label>
			<div class='col-md-4'>
				<!-- Name Input-->
				<div class='form-group'>
					<label class='col-md-4 control-label' for='dishName'>Dish name:</label>
						<div class='col-md-4'>
							<input id='$d' name='dishName' type='search' placeholder='' class='form-control input-md' readonly>
						</div>
					</div>
				<br>
					Number of dishes you want to order:
				<input id = '$c' name='$c' type='number' placeholder='' class='form-control input-md'>
				<span id='output-area1'></span>
				
			</div>
		</div>
<br><br>

<script>
document.getElementById('$d').value = '$y';
document.getElementById('$c').value = 0;
</script>

	";
	$i = $i + 1;

}
?>



<form class="form-horizontal">
<fieldset>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="orderSubmit"></label>
  <div class="col-md-4">
    <button id="orderSubmit" name="orderSubmit" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
</form>

</form>

 </BODY>
</HTML>
