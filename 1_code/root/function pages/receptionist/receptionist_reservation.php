<!-- written by: Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<?php
$servername = "localhost";
$username = "root";
$password = "usbw";

$conn = mysqli_connect($servername, $username, $password, "test");

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$lastName = test_input($_POST["last_Name"]);
		$firstName = test_input($_POST["first_Name"]);
		$phoneNum = test_input($_POST["phone"]);
		$emailAddress = test_input($_POST["email"]);
		$partySize = test_input($_POST["size"]);
		$month = test_input($_POST["month"]);
		if (strcmp($month, "Jan") == 0){
			$month = "01";
		}
		elseif (strcmp($month, "Feb") == 0){
			$month = "02";
		}
		elseif (strcmp($month, "Mar") == 0){
			$month = "03";
		}
		elseif (strcmp($month, "Apr") == 0){
			$month = "04";
		}
		elseif (strcmp($month, "May") == 0){
			$month = "05";
		}
		elseif (strcmp($month, "Jun") == 0){
			$month = "06";
		}
		elseif (strcmp($month, "Jul") == 0){
			$month = "07";
		}
		elseif (strcmp($month, "Aug") == 0){
			$month = "08";
		}
		elseif (strcmp($month, "Sep") == 0){
			$month = "09";
		}
		elseif (strcmp($month, "Oct") == 0){
			$month = "10";
		}
		elseif (strcmp($month, "Nov") == 0){
			$month = "11";
		}
		elseif (strcmp($month, "Dec") == 0){
			$month = "12";
		}
		$day = test_input($_POST["day"]);
		$ampm = test_input($_POST["am_Pm"]);
		$hour = test_input($_POST["hour"]);
		if (strcmp($ampm, "pm") == 0){
			$hour= strval(intval($hour) + 12);
		}
		$minute = test_input($_POST["minute"]);
		$scheduleTime = "2021-" .$month ."-" .$day ." " .$hour .$minute .":00";
		$sql = "INSERT INTO reservation_db (reservationID, scheduleTime, firstName, lastName, phoneNum, emailAddress, partySize) VALUES (NULL, '$scheduleTime', '$firstName', '$lastName', '$phoneNum', '$emailAddress', '$partySize')";
		$result = mysqli_query($conn, $sql);
		echo file_get_contents("receptionist_reservation_success.html");

	}

function test_input($data) {
	$data = trim($data);
  	$data = stripslashes($data);
  	$data = htmlspecialchars($data);
  	return $data;
}
?>