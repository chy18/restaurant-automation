<!-- written by: Xiaotian Zhou, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->
 
<HTML>
 <HEAD>
  <TITLE>Receptionist: View Order</TITLE>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

 </HEAD>
 
 <BODY>

 <form class="form-horizontal" action = "../../position pages/receptionist.php">
<fieldset>
<legend>View Order</legend>

<!--RETURN Button -->
<form class="form-horizontal">
<div class="form-group">
  <label class="col-md-4 control-label" for="return" style = "float: right"></label>
  <div class="col-md-4">
    <button id="return" name="return" class="btn btn-primary">Exit</button>
  </div>
</div>
</form>

</fieldset>
</form>

  <table width="100%" border="8 solid" rules="all" align="center" cellpadding="3">
  <tr>
<th>Order ID:</th>
<th>Time of order:</th>
<th>TIME REMAINING:</th>
<th>Dishes in the order:</th>
<th>Price of the order:</th>
  </tr>

<?php
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM order_db");

  while ($rows = $resultSet->fetch_assoc()){
    if ($rows['isCurrent'] == 1 or $rows['isServed'] == 0){
      $orderID = $rows['orderID'];
      $orderTime = $rows['orderTime'];
      date_default_timezone_set('America/New_York');
      $currentTime = date("Y-m-d");
      $currentTime .= " ";
      $currentTime .= date("H:i:s");
      $mins = (int)((strtotime($currentTime) - strtotime($orderTime))/60);
      $orderDishes = $rows['orderDishes'];
      $orderEstimate = $rows['orderEstimate'];
      $mins = (int)($orderEstimate - $mins);
      $orderPrice = $rows['orderPrice'];
      if ($rows['isServed'] == 1){
        $mins = 'served!';
      }
      else{
        $mins .= " min";
      }

      echo "
        <tr>
    <td>$orderID</td>
    <td>$orderTime</td>
    <td>$mins</td>
    <td>$orderDishes</td>
    <td>\$$orderPrice.00</td>
        </tr>

      ";
    }
    
  }

?>




 
  </table>

 

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $order = test_input($_POST["chooseOrder"]);
  include "../../includes/php_scripts/db_connect.php";
  $sql = "UPDATE order_db SET isServed='1' WHERE orderID = '$order'";
  $resultSet = $mysqli->query($sql);
  header("Refresh:0");
}
function test_input($data) {
  $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


 </BODY>
</HTML>
