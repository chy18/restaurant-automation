<!-- written by: Zerui Li, Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<html>

<head>
  <title>Receiptionist: Payment</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>

<body>

<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<fieldset>

<!-- Form Name -->
<legend>Payment</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="chooseOrder">Choose Order</label>
  <div class="col-md-4">
    

      <?php
        include "../../includes/php_scripts/db_connect.php";
        $resultSet = $mysqli->query("SELECT * FROM order_db");
      ?>

      <select id="chooseOrder" name="chooseOrder" class="form-control">

      <?php
        while ($rows = $resultSet->fetch_assoc()) {
        $orderID = $rows['orderID'];
        $name = "Order " .$orderID;
        if ($rows['isCurrent'] == true){
          echo "<option value = '$orderID'>$name</option>";
        }
      }

      ?>

    </select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="confirm"></label>
  <div class="col-md-8">
    <button type="submit" id="confirm" name="confirm" class="btn btn-primary">Confirm</button>
  </div>
</div>
</form>

<!-- Button -->
<form class="form-horizontal" method="post" action="../../position pages/receptionist.php">
<div class="form-group">
  <label class="col-md-4 control-label" for="confirm"></label>
  <div class="col-md-8">
    <button type="submit" id="confirm" name="confirm" class="btn btn-danger">Cancel</button>
  </div>
</div>
</form>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="payment">Payment $</label>  
  <div class="col-md-4">
    <input id='payment' name='payment' type='text' placeholder='' class='form-control input-md' readonly>

  <?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $order = test_input($_POST["chooseOrder"]);
  include "../../includes/php_scripts/db_connect.php";
  $resultSet = $mysqli->query("SELECT * FROM order_db");
  while ($rows = $resultSet->fetch_assoc()) {
    if ((strcmp($rows['orderID'], $order) == 0) && $rows['isCurrent'] == true){
      $num = $rows['orderID'];
      $x = $rows['orderPrice'];
      session_start();
      $_SESSION['order'] = $num;
      echo "
      <div class='form-group'>
      <label class='col-md-4 control-label' for='cash'></label>
      <div class='col-md-8'>
      <br>
      <form method='post' class='form-horizontal' action = 'receptionist_payment_cash_success.php'>
      <button type='submit' id='cash' name='cash' class='btn btn-primary'>Cash</button>
      </form>
      <form method='post' class='form-horizontal' action = 'receptionist_payment_credit_success.php'>
      <button type='submit' id='credit' name='credit' class='btn btn-primary'>Credit</button>
      </form>
      </div>
      </div>


      <script> 
      document.getElementById('payment').value='$x' ; 
      </script>
      ";
      break;
    } 
  }

}
function test_input($data) {
  $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>
    
  </div>
</div>


</fieldset>
</body>
</html>



