<!-- written by: Christopher Yeh-->
<!-- tested by: Christopher Yeh-->
<!-- debugged by: Christopher Yeh-->

<?php

$servername = "localhost";
$username = "root";
$password = "usbw";

$conn = mysqli_connect($servername, $username, $password, "test");

if (!$conn){
	die("Connection failed: " . $conn -> mysqli_connect_error());
}
else{
	echo "SQL Database: Connect Successfully!";
}

$sql = "SELECT * FROM employee_db";
$result = mysqli_query($conn, $sql);

$username = $password = $error = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$username = test_input($_POST["username"]);
	$password = test_input($_POST["password"]);
	while ($row = mysqli_fetch_assoc($result)){
		if (strcmp($row['employeeUsername'], $username) == 0 && strcmp($row['employeePassword'], $password) == 0){
			$position = $row['employeeType'];
			$location = "Location: position pages/" .$position .".html";
			header($location);
			break;
		}
	}
	echo '<script type = "text/JavaScript">
		alert("Credential fails, please try again");
		</script>';
	echo file_get_contents("loginPage.html");

}

function test_input($data) {
	$data = trim($data);
  	$data = stripslashes($data);
  	$data = htmlspecialchars($data);
  	return $data;
}
?>